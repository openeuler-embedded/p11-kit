Name:           p11-kit
Version:	0.23.20
Release:	2
Summary:        Provides a way to load and enumerate PKCS#11 modules.
License:        BSD
URL:            http://p11-glue.freedesktop.org/p11-kit.html

Source0:        https://github.com/p11-glue/p11-kit/releases/download/%{version}/%{name}-%{version}.tar.xz
Source1:        p11-kit-client.service

Patch0:         backport-CVE-2020-29363-Check-attribute-length-against-buffer-size.patch
Patch1:         backport-CVE-2020-29362-Fix-bounds-check-in-p11_rpc_buffer_get_byte_array.patch
Patch2:         backport-proxy-Fix-slot-ID-reuse-avoiding-duplicating-IDs.patch
Patch3:         backport-0001-CVE-2020-29361-Check-for-arithmetic-overflows-before-allocating.patch
Patch4:         backport-0002-CVE-2020-29361-Follow-up-to-arithmetic-overflow-fix.patch 

BuildRequires:  gcc libtasn1-devel >= 2.3 libffi-devel gtk-doc systemd-devel pkgconfig(glib-2.0) libxslt
BuildRequires:  bash-completion

Provides:       p11-kit-server = %{version}-%{release}
Obsoletes:      p11-kit-server < %{version}-%{release}

%description
Provides a way to load and enumerate PKCS#11 modules.
Provides a standard configuration setup for installing
PKCS#11 modules in such a way that they're discoverable.
Also solves problems with coordinating the use of PKCS#11
by different components or libraries living in the same process.

%package devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
Provides header and libraries files for applications use %{name} to develop.

%package server
Summary:        Server and client commands for %{name}
Requires:       %{name} = %{version}-%{release}

%description server
The %{name}-server package contains command line tools that enable to
export PKCS#11 modules through a Unix domain socket. Note that this 
feature is still experimental.

%package trust
Summary:        Trust policy module of %{name}
Requires:       %{name} = %{version}-%{release}
Conflicts:      nss < 3.14.3-9

%description trust
This package contains PKCS#11 trust policy module.

%package trust-tools
Summary:        Tools for trust policy module of %{name}
Requires:       %{name} = %{version}-%{release}
Conflicts:      nss < 3.14.3-9

%description trust-tools
This package contains PKCS#11 tools for trust policy module.

%package help
Summary:        Help infomation of %{name}
Requires:       %{name} = %{version}-%{release}

%description help
This package contains help information of p11-kit.

%define multilib_arches ppc64 sparc64 x86_64 ppc64le
%ifarch %{multilib_arches}
%define alt_ckbi  libnssckbi.so.%{_arch}
%else
%define alt_ckbi  libnssckbi.so
%endif

%prep
%autosetup -p1

%build
%configure --enable-doc --with-trust-paths=/etc/pki/ca-trust/source:/usr/share/pki/ca-trust-source
make -j 4 V=1

%install
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pkcs11/modules
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_userunitdir}
find $RPM_BUILD_ROOT -type f -name "*.la" -delete -print

%check
make check

%post -p /sbin/ldconfig

%post trust
%{_sbindir}/update-alternatives --install %{_libdir}/libnssckbi.so %{alt_ckbi} %{_libdir}/pkcs11/p11-kit-trust.so 30

%postun -p /sbin/ldconfig

%postun trust
if [ $1 -eq 0 ]; then
        %{_sbindir}/update-alternatives --remove %{alt_ckbi} %{_libdir}/pkcs11/p11-kit-trust.so
fi
 
%files
%license COPYING
%dir %{_libexecdir}/p11-kit
%dir %{_sysconfdir}/pkcs11
%dir %{_sysconfdir}/pkcs11/modules
%dir %{_datadir}/p11-kit
%dir %{_datadir}/p11-kit/modules
%{_bindir}/p11-kit
%{_libdir}/libp11-kit.so.*
%{_libdir}/p11-kit-proxy.so
%{_libexecdir}/p11-kit/p11-kit-remote
%{_datadir}/bash-completion/completions/p11-kit

%files server
%{_userunitdir}/p11-kit-client.service
%{_userunitdir}/p11-kit-server.service
%{_userunitdir}/p11-kit-server.socket
%{_libdir}/pkcs11/p11-kit-client.so
%{_libexecdir}/p11-kit/p11-kit-server

%files help
%doc AUTHORS NEWS README
%doc p11-kit/pkcs11.conf.example
%{_mandir}/man1/trust.1.gz
%{_mandir}/man8/p11-kit.8.gz
%{_mandir}/man5/pkcs11.conf.5.gz
%{_sysconfdir}/pkcs11/pkcs11.conf.example

%files devel
%doc %{_datadir}/gtk-doc/
%{_includedir}/p11-kit-1/
%{_libdir}/libp11-kit.so
%{_libdir}/pkgconfig/p11-kit-1.pc

%files trust
%dir %{_libdir}/pkcs11
%ghost %{_libdir}/libnssckbi.so
%{_libdir}/pkcs11/p11-kit-trust.so
%{_datadir}/p11-kit/modules/p11-kit-trust.module
%{_libexecdir}/p11-kit/trust-extract-compat

%files trust-tools
%{_bindir}/trust
%{_datadir}/bash-completion/completions/trust

%changelog
* Sat Jan 9 2021 zoulin <zoulin13@huawei.com> - 0.23.20-2
- fix CVE-2020-29361 CVE-2020-29362 CVE-2020-29363

* Thu Aug 27 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 0.23.20-1
- update version to 0.23.20

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-6
- delete unused file

* Sat Dec 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-5
- fix update problem

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-4
- Add missing Requires

* Wed Nov 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-3
- Add Buildrequires: libxslt

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-2
- Correct patch number

* Sat Sep 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.23.14-1
- Package init
